//
//  SwiftUI_WeatherAppApp.swift
//  SwiftUI-WeatherApp
//
//  Created by Amit Pandey on 25/01/2024.
//

import SwiftUI

@main
struct SwiftUI_WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
