//
//  ContentView.swift
//  SwiftUI-WeatherApp
//
//  Created by Amit Pandey on 25/01/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            LinearGradient(colors: [.blue, .white],
                           startPoint: .topLeading,
                           endPoint: .bottomTrailing)
            .ignoresSafeArea()
            
            VStack {
                
                Text("Cupertino, CA")
                    .foregroundStyle(.white)
                    .font(.system(size: 35, weight: .bold))
                    .padding()
                
                VStack {
                    
                    Image(systemName: "cloud.sun.fill")
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 200, height: 160)
                    
                    Text("76°")
                        .font(.system(size: 50, weight: .bold))
                        .foregroundStyle(.white)
                    
                }
                .padding(.top, 50)
                
                HStack(spacing: 20) {
                    VStack {
                        Text("TUE")
                            .foregroundStyle(.white)
                            .font(.system(size: 17, weight: .medium))
                        
                        Image(systemName: "cloud.sun.fill")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width:50, height: 50)
                            .padding(.top, 5)
                        
                        Text("74°")
                            .font(.system(size: 24, weight: .semibold))
                            .foregroundStyle(.white)
                    }
                    VStack {
                        Text("WED")
                            .foregroundStyle(.white)
                            .font(.system(size: 17, weight: .medium))
                        
                        Image(systemName: "sun.max.fill")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width:50, height: 50)
                            .padding(.top, 5)
                        
                        Text("70°")
                            .font(.system(size: 24, weight: .semibold))
                            .foregroundStyle(.white)
                    }
                    VStack {
                        Text("THU")
                            .foregroundStyle(.white)
                            .font(.system(size: 17, weight: .medium))
                        
                        Image(systemName: "wind")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width:50, height: 50)
                            .padding(.top, 5)
                        
                        Text("60°")
                            .font(.system(size: 24, weight: .semibold))
                            .foregroundStyle(.white)
                    }
                    VStack {
                        Text("FRI")
                            .foregroundStyle(.white)
                            .font(.system(size: 17, weight: .medium))
                        
                        Image(systemName: "sun.horizon.fill")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width:50, height: 50)
                            .padding(.top, 5)
                        
                        Text("60°")
                            .font(.system(size: 24, weight: .semibold))
                            .foregroundStyle(.white)
                    }
                    VStack {
                        Text("SAT")
                            .foregroundStyle(.white)
                            .font(.system(size: 17, weight: .medium))
                        
                        Image(systemName: "moon.stars.fill")
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width:50, height: 50)
                            .padding(.top, 5)
                        
                        Text("55°")
                            .font(.system(size: 24, weight: .semibold))
                            .foregroundStyle(.white)
                    }
                }
                .padding()
                
                Spacer()
                
                Text("Change Time of Day")
                    .padding()
                    .padding(.leading, 20)
                    .padding(.trailing, 20)
                    .background(.white)
                    .foregroundColor(.blue)
                    .clipShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
                    .font(.system(size: 20, weight: .bold))
                
                Spacer()
                
            }
            
        }
    }
}

#Preview {
    ContentView()
}
